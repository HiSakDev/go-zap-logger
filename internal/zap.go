package internal

import (
	"io"
	"os"
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var (
	Logger                   *zap.Logger
	ZapLogSamplingOpts       []zapcore.SamplerOption
	ZapLogSamplingTick       = time.Second
	ZapLogSamplingInitial    = 100
	ZapLogSamplingThereafter = 100
)

func init() {
	Logger = NewLogger(os.Stderr, zapcore.InfoLevel, nil)
}

func NewLogger(w io.Writer, level zapcore.Level, enc zapcore.Encoder, opts ...zap.Option) *zap.Logger {
	if enc == nil {
		cfg := zapcore.EncoderConfig{
			TimeKey:        "ts",
			LevelKey:       "level",
			NameKey:        "logger",
			CallerKey:      "caller",
			FunctionKey:    zapcore.OmitKey,
			MessageKey:     "msg",
			StacktraceKey:  "stacktrace",
			LineEnding:     zapcore.DefaultLineEnding,
			EncodeLevel:    zapcore.LowercaseLevelEncoder,
			EncodeTime:     zapcore.TimeEncoderOfLayout("2006/01/02 15:04:05.000000"),
			EncodeDuration: zapcore.StringDurationEncoder,
			EncodeCaller:   zapcore.ShortCallerEncoder,
		}
		enc = zapcore.NewConsoleEncoder(cfg)
	}
	core := zapcore.NewCore(enc, zapcore.AddSync(w), zap.NewAtomicLevelAt(level))
	core = zapcore.NewSamplerWithOptions(core, ZapLogSamplingTick, ZapLogSamplingInitial, ZapLogSamplingThereafter, ZapLogSamplingOpts...)
	return zap.New(core, append([]zap.Option{zap.WithCaller(true)}, opts...)...)
}
