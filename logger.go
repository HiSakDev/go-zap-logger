package logger

import (
	"io"
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	"gitlab.com/HiSakDev/go-zap-logger/internal"
)

func SetZapLogSampling(tick time.Duration, first, thereafter int, opts ...zapcore.SamplerOption) {
	internal.ZapLogSamplingTick = tick
	internal.ZapLogSamplingInitial = first
	internal.ZapLogSamplingThereafter = thereafter
	internal.ZapLogSamplingOpts = opts
}

func SetLogger(w io.Writer, level zapcore.Level, enc zapcore.Encoder, opts ...zap.Option) *zap.Logger {
	internal.Logger = internal.NewLogger(w, level, enc, opts...)
	return internal.Logger
}

func GetLogger() *zap.Logger {
	return internal.Logger
}
